PROGRAM Bisection (INPUT,OUTPUT);

CONST
	tol = 5E-8;

TYPE
	intpos = 1..maxint;

VAR
	x, fx : REAL;
	iters : intpos;
	exito : BOOLEAN;

{FUNCTION f(x : REAL) : REAL;
	BEGIN
		f := x - exp(1/x);
	END;
}
FUNCTION f(x: REAL) : REAL;
	BEGIN
		f := x-exp(1/x);
	END;

PROCEDURE Biseccion(x0, x1, tolx : REAL; maxiter : intpos;VAR raiz, fraiz : REAL;VAR numiter : intpos;VAR converge : BOOLEAN);

	VAR
		x,fx,f0 : REAL;
		contiter : 0..maxint;
 		estado : (iterando,dentrodetol,maxiteralcanzado);

	BEGIN
		f0 := f(x0);
 		contiter := 0;
		estado := iterando;
		
		REPEAT
			contiter := contiter + 1;
			x := (x0 + x1)/2;
			fx := f(x);
		
			IF f0*fx <= 0 THEN x1 := x
		 	ELSE
				BEGIN
					x0 := x;
					f0 := fx;
				END;
			
			IF x1-x0 <= tolx THEN estado := dentrodetol
			ELSE
				IF contiter = maxiter THEN estado := maxiteralcanzado
			
		until estado <> iterando;
		converge := estado = dentrodetol;
		raiz := (x0 + x1)/2;
		fraiz := f(raiz);
		numiter := contiter;	

	END;

BEGIN
{	x:=0;fx:=0;iters:=0;exito:=true;}
	biseccion(0.5,2,tol,50,x,fx,iters,exito);
	WRITELN ('Raiz:  ',x);
	WRITELN ('Valor:  ',fx);
	WRITELN ('Iteraciones  ',iters);
	WRITELN ('Logrado  ',exito);
END.
