PROGRAM Secante1;
	CONST
		tol = 5E-10;
	TYPE	
		intPos = 0..maxint;
		estadoSec = (iterando,dentroTol,maxIterAlcanzado,casiCero,muyPlana);

	VAR
		x, fx : REAL;
		resultado : estadoSec;
		numIter : intPos;

FUNCTION f(x : REAL) : REAL;
	BEGIN
		f := x-exp(1/x);
	END;

PROCEDURE Secante1(x0, x1, tolx :REAL;
                   maxIter :intPos;
				   VAR raiz, fraiz:REAL;
                   VAR numIter:intPos;
                   VAR resultado:estadoSec);
	CONST
		supuestoCero = 1E-20;
	VAR
		f0,f1,x2 : REAL;
		contIter : intPos;
		estado : estadoSec;
		
	BEGIN
		f0 := f(x0);
		f1 := f(x1);
		contIter := 0;
		estado := iterando;

		REPEAT
			IF abs(x1) <= supuestoCero THEN estado := casiCero
			ELSE
				IF abs(f1 - f0) <= supuestoCero THEN estado := muyPlana
				ELSE
					BEGIN
						contIter := contIter + 1;
						x2 := x1 -(x1-x0)*f1/(f1-f0);
						if abs((x2-x1)/x1) <= tolx THEN estado := dentroTol
						ELSE
							BEGIN 
								x0 := x1;
								x1:= x2;
								f0 := f1;
								f1 := f(x1);
							END;
					END;
		UNTIL estado <> iterando;	
		
		resultado := estado;
		raiz := x2;
		fraiz := f(x2);
		numIter := contIter;
	END;

BEGIN
	Secante1(1,2,tol,50,x,fx,numIter,resultado);
  	WRITELN ('Raiz:  ',x);
    WRITELN ('Valor:  ',fx);
    WRITELN ('Iteraciones  ',numIter);
    WRITELN ('estado final  ',resultado);
	
END.
