PROGRAM Secante2;

CONST
	tol = 5E-10;
TYPE
	intPos = 0..maxint;
	estadoSec = (iterando,dentroTol,maxIterAlcanzado,casiCero,muyPlana);
VAR
	x,fx : REAL;
	iter : intPos;
	estado : estadoSec;

FUNCTION f(x : REAL) :REAL;
	BEGIN
		f := x-exp(1/x);
	END;

PROCEDURE Secante(x0,x1,tolx : REAL;
									maxIter : intPos;
									VAR raiz,fraiz : REAL;
									VAR numDeIter : intPos;
									VAR resultado : estadoSec);
	
	CONST
		supuestoCero = 1E-20;
	VAR
		f0,f1 : REAL;
		contIter : intPos;
		x0esActual : BOOLEAN;
		estado : estadoSec;

	BEGIN
		f0 := f(x0);
		f1 := f(x1);
		x0esActual := false;
		contIter := 0;
		estado := iterando;

		REPEAT
			IF (abs(f1 - f0) <= supuestoCero) THEN estado := muyPlana 
			ELSE
				BEGIN
					contIter := contIter + 1;
					IF x0esActual THEN
						BEGIN
							x1 := x1 - (x1 - x0)*f1/(f1-f0); 
							f1 := f(x1);
						END	 
					ELSE
						BEGIN
							x0 := x0 - (x0 - x1)*f0/(f0-f1);
							f0 := f(x0);
						END;
						x0esActual := not x0esActual;
						IF  (abs(x0) <= supuestoCero) THEN estado := casiCero
						ELSE
							IF (abs((x1-x0)/x0) <= tolx) THEN estado := dentroTol 
							ELSE
								IF contIter = maxIter THEN estado := maxIterAlcanzado
				END;
		until estado <> iterando;
		resultado := estado;
		IF x0esActual THEN
			BEGIN
				raiz :=x0;
				fraiz := f0;
			END
		ELSE
			BEGIN
				raiz := x1;
				fraiz := f1;
			END;
		numDeIter := contIter;
	END;

BEGIN
	Secante(1,2,tol,50,x,fx,iter,estado);
	WRITELN('Raiz: ',x);
	WRITELN('Valor de fucnion en raiz: ',fx);
	WRITELN('Numero de iteraciones: ',iter);
	WRITELN('Estado: ',estado);
END.
