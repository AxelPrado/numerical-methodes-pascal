PROGRAM CalculaRaizCuad (input,output);
{#Programa que encuentra la raiz cuadrada de un conjunto de números dados en un archivo.
#El archivo debe contener el valor inicial.
#El archivo debe tener la tolerancia.
#El archivo debe tener el limite sobre el número de iteraciones permitidas para cada número.}

CONST
	maxiter=50;	{Definimos el máximo de iteraciones (valor constante)}

TYPE	
	contiters=0..maxiter;	{Definimos el tipo contador de iteraciones}	

VAR
	arch, archivo:TEXT;	{Definimos la varible que referenciará al archivo donde están los números a calcular la raiz y el archivo donde 				se escriben los resultados}
	numero, valini, raizcuad, tolerancia:REAl;	{se definen los identificadores de los campos}
	iter, limite:contiters;	{Definimos contadores de iteraciones}
	resuelto:BOOLEAN;
	archent:STRING;

PROCEDURE HallaRaizCuad (v,x,tol:REAL; limiter:contiters; VAR exito:BOOLEAN; VAR raiz:REAL; VAR numdeiters:contiters);	
	VAR
		contiter:contiters;
		estado:(probando,hecho,muchasits);

	BEGIN 
		contiter:=0; estado:=probando;
		REPEAT
			IF ABS(SQR(x)-v)<=tol THEN estado:=hecho ELSE
				IF contiter=limiter THEN estado:=muchasits ELSE
					BEGIN
						x:=(x+v/x)/2;
						contiter:=contiter+1;
					END;
		UNTIL estado<>probando;
		exito:=estado=hecho;
		IF exito THEN
			BEGIN
				raiz:=x;
				numdeiters:=contiter;
			END;
	END;

BEGIN
	WRITELN (' Este programa encuentra la raiz cuadrada de una secuencia');
	WRITELN (' de valores dados en un archivo externo.');
	WRITELN (' Escribe el nombre del archivo en que estan los datos:');
	READLN (archent);
	ASSIGN (arch,'Archivo1.txt');
	REWRITE (arch);
	WRITELN (arch,'número		     valor	  tolerancia	   lím de	    raiz		   iteraciones');
	WRITELN (arch,'           	    inicial		 	  iters');
	WRITELN;
	ASSIGN (archivo,archent);
	RESET (archivo);
	REPEAT
		READLN (archivo,numero,valini,tolerancia,limite);
		WRITE (arch,numero:10:6,'	  ', valini:10:6,'	  ', tolerancia:8,'	', limite:7);
		HallaRaizCuad (numero, valini, tolerancia, limite, resuelto, raizcuad, iter);
		IF resuelto THEN WRITELN (arch,'		',raizcuad:13:6,'		 ', iter:8) ELSE
			WRITELN (arch,'		','	--				--	')
	until eof (archivo);
	CLOSE (arch);
	CLOSE (archivo);
		WRITELN;
		WRITELN ('Se han procesado todos los datos');
END.

